                                                        
# NSCOMM 

## Database Connection 

Database Connection 은 application-{profile}.yml 에서 설정을 하며 dev,local,service 로 관리 하며 운영상 더 추가 될 수 있음.
Connection 을 추가 할 경우 src.config 에 SqlMapper 설정과 Datasource 설정의 class 파일을 추가 한다.
구조적으로 Database 기준으로 package 와 xml 디렉토리를 나눈다.

## logging

logback-spring.xml 로 설정을 하며 spring profile 정보로 log 남기는 부분을 설정 한다

## SqlMapper

query xml 과 Mapper interface 는 통일되게 네이밍을 작성 하되 Mapper 는 뒤에 Mappers 의 Suffix 를 붙임

* query xml : project/src/main/resources/sqlMappers/{database name}/{behavior}.xml
* Mapper interface : project-path/src/main/java/com/oksusu/nscomm/admin:wm/repository/mappers/{database name}/{behaviot}Mappers.java 
