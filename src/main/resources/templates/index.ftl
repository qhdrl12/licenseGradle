<!DOCTYPE HTML>
<html>
<head>
<#--<#include "common/head.ftl">-->
    <title>Channel License</title>
    <link rel="stylesheet" href="/public/libs/bootstrap/css/bootstrap.css"/>
    <link rel="stylesheet" href="/public/libs/jquery/themes/smoothness/jquery-ui.css"/>

    <script src="/public/libs/jquery/jquery-1.12.0.min.js"></script>
    <script src="/public/libs/bootstrap/assets/js/respond.min.js"></script>
    <script src="/public/libs/bootstrap/js/bootstrap.min.js"></script>

    <style text="text/css">
        #loading{
            display: none;
            width: 100%;
            text-align: center;
        }
        #resultArea{
            display: none;
            margin: 20px;
        }

        #resultArea label{
            color: blue;
        }

        #ch_data{
            -ms-word-wrap: ;
            word-wrap: break-word;
            width: 800px;
            height: 200px;
            overflow-y: auto;
        }

        #spreadBtn{
            margin-left:15px;
            margin-top:15px;
        }
    </style>
</head>
<body>
<#--<#include "common/header.ftl">-->
<#--<#include "common/side.ftl">-->

<h1> Channel License Index Page</h1>
<div class="row" style="padding-left: 30px;">
    <h3>1. IF-LICENSE-001</h3>
    <!--(<a href="iptvchlicense/search?stb_id={5EA18A4F-DE5C-11E4-B1E2-83BD0173DE6D}&mac_address=28:32:c5:4f:2c:f9">Default 링크</a>)-->
    <div class="col-sm-12">
        <div class="btn-group btn-group-sm">
            <div class="col-sm-6">
                <label for="stb_id">stb_id :</label>
                <input type="text" class="form-control" id="stb_id" size="40" placeholder="{5EA18A4F-DE5C-11E4-B1E2-83BD0173DE6D}"/>
            </div>
            <div class="col-sm-3">
                <label for="mac_address">mac_address :</label>
                <input type="text" class="form-control" id="mac_address" size="40" placeholder="28:32:c5:4f:2c:f9"/>
            </div>
            <div class="col-sm-2">
                <button class="btn btn-default btn-xs" onclick="clickSearch();" style="margin-top: 33px;">조회</button>
                <button class="btn btn-default btn-xs" onclick="initResult();" style="margin-top: 33px;">초기화</button>
            </div>
        </div>
    </div>

    <div class="col-sm-12" id="loading"><img src="/public/img/loader.gif" width="100" height="100"></div>

    <div class="col-sm-12"><input type="button" class="btn btn-primary" id="spreadBtn" value="결과 접기"/></div>
    <div class="col-sm-12" id="resultArea">
        <label for="access">DB 조회 여부 :</label>
        <div id="access"></div>
        <label for="result">Result :</label>
        <div id="result"></div>
        <label for="ch_data">Channel List Auth :</label>
        <div id="ch_data"></div>
    </div>


</div>

<#--<#include "common/footer.ftl">-->
</body>

<script type="text/javascript">
    $(document).ready(function(){
        $(document).ajaxStart(function(){
           $("#resultArea").hide();
           $("#loading").show();
        }).ajaxStop(function () {
           $("#loading").hide();
           $("#resultArea").show();
        });

        $("#spreadBtn").click(function () {
           if($("#resultArea").is(":visible")){
               $("#resultArea").slideUp();
           }else{
               $("#resultArea").slideDown();
           }
        });
    });

    var result_arr = ["access", "ch_data", "result"];

    var clickSearch = function(){
        initResult();

        var stbId = '{5EA18A4F-DE5C-11E4-B1E2-83BD0173DE6D}';
        var macAddr = '28:32:c5:4f:2c:f9';
        var fields = ["access", "result", "ch_data"];

        if($('#stb_id').val() && $('#mac_address').val()){
            stbId = $('#stb_id').val();
            macAddr = $('#mac_address').val();
        }

        //var url = 'iptvchlicense/search';
        var url = 'iptvchlicense/stbs';
        var param = {
            stb_id : stbId,
            mac_address : macAddr
        }

        $.ajax({
            contentType: 'application/json'
            ,url: url + "/" + stbId + "/" + macAddr + "?fields=" + fields
            , type: "get"
//            , data: JSON.stringify(param)
            , dataType: "xml"
        }).done(function (data) {
            setResult(data);
        })
    }

    var initResult = function(){
        $("#access").empty();
        $("#ch_data").empty();
        $("#result").empty();
    }

    var setResult = function(data){
        if($(data).find("root").length > 0){
            result_arr.forEach(function(key){
                $("#"+key).append($(data).find(key).text());
            })
        }else{
            $("#result").append("호출 실패");
        }
    }
</script>
</html>