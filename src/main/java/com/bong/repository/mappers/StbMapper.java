package com.bong.repository.mappers;

import com.bong.domain.data.LicenseInfoOrigin;
import com.bong.domain.data.StbUserDetailInfo;
import com.bong.domain.request.StbInfoSearchParam;
import org.springframework.stereotype.Repository;

/**
 * Created by ibong-gi on 2016. 12. 5..
 */

@Repository
public interface StbMapper {
    StbUserDetailInfo getStbInfo(StbInfoSearchParam stbInfoSearchParam);
    LicenseInfoOrigin getLicenseInfo(StbInfoSearchParam stbInfoSearchParam);
}
