package com.bong.repository.mappers;

import com.bong.domain.data.BasePackageInfo;
import com.bong.domain.data.StbUserInfo;
import com.bong.domain.data.mongo.EventChannelDataCollection;
import com.bong.domain.data.mongo.MappingChannelDataCollection;
import com.bong.domain.data.mongo.PayChannelDataCollection;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by ibong-gi on 2017. 1. 13..
 */

@Repository
public interface ProcessMapper {
    List<BasePackageInfo> getPackageChannel();
    List<PayChannelDataCollection> getAllPayData(List<String> tvPackages);
    List<EventChannelDataCollection> getEventData();
    String getMappingChannelList();
    List<StbUserInfo> getAllStbs();
}