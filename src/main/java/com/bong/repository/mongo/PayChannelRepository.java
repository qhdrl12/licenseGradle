package com.bong.repository.mongo;

import com.bong.domain.data.mongo.PayChannelDataCollection;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

/**
 * Created by ibong-gi on 2017. 1. 16..
 */
public interface PayChannelRepository extends MongoRepository<PayChannelDataCollection, String> {

    @Query(value = "{ '_id' : ?0 }")
    PayChannelDataCollection findById(String id);
}
