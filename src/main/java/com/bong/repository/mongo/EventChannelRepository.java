package com.bong.repository.mongo;

import com.bong.domain.data.mongo.EventChannelDataCollection;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by ibong-gi on 2017. 1. 16..
 */
public interface EventChannelRepository extends MongoRepository<EventChannelDataCollection, String> {
}
