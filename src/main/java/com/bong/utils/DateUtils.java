package com.bong.utils;

import lombok.extern.slf4j.Slf4j;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.text.ParseException;

/**
 * Created by ibong-gi on 2017. 1. 13..
 */
@Slf4j
public class DateUtils {
    public static final String FORMAT_YYYYMMDDHHMMSSSSS = "yyyyMMddHHmmssSSS";
    public static final String FORMAT_YYYYMMDDHHMMSS = "yyyyMMddHHmmss";
    public static final String FORMAT_YYMMDDHHMMSS = "yyMMddHHmmss";
    public static final String FORMAT_YYYYMMDDHHMM = "yyyyMMddHHmm";
    public static final String FORMAT_YYYYMMDD = "yyyyMMdd";
    public static final String FORMAT_YYYYMMDD_DASH = "yyyy-MM-dd";
    public static final String FORMAT_YYYYMMDDHHMMSS_SLASH_COLON = "yyyy/MM/dd HH:mm:ss";
    public static final String FORMAT_YYYYMMDDHHMMSS_HYPHEN_COLON = "yyyy-MM-dd HH:mm:ss";
    public static final String FORMAT_YYYYMM = "yyyyMM";


    private DateUtils() {}

    public static String format(final String format, Date date) {
        return new SimpleDateFormat(format).format(date);
    }

    public static String convertStringDateFormat(final String date,
                                                 final String parseFormat, final String format) {

        String result;

        try {
            SimpleDateFormat parseSdf = new SimpleDateFormat(parseFormat);
            SimpleDateFormat formatSdf = new SimpleDateFormat(format);

            Date parseAuthDate = parseSdf.parse(date);

            result = formatSdf.format(parseAuthDate);
        } catch (Exception ignore) {
            result = date;
        }

        return result;
    }

    public static String getTodayString() {
        return format(FORMAT_YYYYMMDD, new Date());
    }

    public static Date toDate(final String date) {
        return toDate(date, FORMAT_YYYYMMDD);
    }

    public static Date toDate(final String date, final String format) {

        Date parseDate = null;
        SimpleDateFormat parseSdf = new SimpleDateFormat(format);

        try {
            parseDate = parseSdf.parse(date);
        } catch (ParseException ignore) {
        }

        return parseDate;
    }
}
