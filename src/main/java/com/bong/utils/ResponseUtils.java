package com.bong.utils;

import com.bong.domain.response.CommonResponse;

/**
 * Created by ibong-gi on 2017. 1. 13..
 */
public class ResponseUtils {

    private ResponseUtils(){}

    public static CommonResponse setFailResponse(CommonResponse commonResponse){
        commonResponse.setResult("FAIL");
        commonResponse.setReason("Invalid Parameter");

        return commonResponse;
    }

    public static CommonResponse setSuccessResponse(CommonResponse commonResponse){
        commonResponse.setResult("YES");
        commonResponse.setReason("");

        return commonResponse;
    }
}
