package com.bong.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;

import java.io.IOException;

/**
 * Created by ibong-gi on 2017. 1. 13..
 */
public class JsonUtils {

    private JsonUtils(){}

    public static <T> String objectToJsonAsString(T t)
            throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(t);
    }

    public static <T> T jsonToObject(final String json, Class<T> tclass)
            throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, tclass);
    }

    public static Object jsonFilter(final String jsonFilter, final String[] filterList, Object ins, Class cls)
            throws IOException {
        ObjectMapper mapper = new ObjectMapper();

        SimpleFilterProvider filters = new SimpleFilterProvider().addFilter(jsonFilter, SimpleBeanPropertyFilter.filterOutAllExcept(filterList));

        filters.setFailOnUnknownId(false); // @JSONFilter Not Found 예외 처리

        ObjectWriter writer = mapper.writer(filters);

        return mapper.readValue(writer.writeValueAsString(ins), cls);
    }
}
