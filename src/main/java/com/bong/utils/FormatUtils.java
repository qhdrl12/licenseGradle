package com.bong.utils;

import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.List;

/**
 * Created by ibong-gi on 2017. 2. 1..
 */
@Slf4j
public class FormatUtils {
    public static String[] stringToArray(String str) {
        String[] result;
        if(str == null){
            log.error("parameter is null");
            result = new String[0];
        }else if(str.indexOf(",") == -1){
            result = new String[]{str};
        }else{
            result = str.split(",");
        }
        return result;
    }

    public static List<String> stringToList(String str){
        return Arrays.asList(stringToArray(str));
    }

    public static String licenseKeyToReturnFormat(String chlist){
        return chlist.replaceAll(":1", ":99991231000000").replaceAll(":0", ":00000000000000");
    }
}
