package com.bong.svc;

import com.bong.domain.request.StbInfoSearchParam;
import com.bong.domain.response.license.LicenseResponseList;

/**
 * Created by ibong-gi on 2016. 12. 6..
 */
public interface LicenseService {
    LicenseResponseList getStbStatus(StbInfoSearchParam stbInfoSearchParam, String[] fields);
    void callRestTemplate(StbInfoSearchParam stbInfoSearchParam);
}
