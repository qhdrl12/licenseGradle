package com.bong.svc;

import com.bong.com.CommonProperty;
import com.bong.com.Constants;
import com.bong.config.AppConfig;
import com.bong.domain.data.BasePackageInfo;
import com.bong.domain.data.StbUserInfo;
import com.bong.domain.data.mongo.BasePackageMapCollection;
import com.bong.domain.data.mongo.EventChannelDataCollection;
import com.bong.domain.data.mongo.MappingChannelDataCollection;
import com.bong.domain.data.mongo.PayChannelDataCollection;
import com.bong.domain.request.DataSetApiParam;
import com.bong.domain.response.CommonResponse;
import com.bong.domain.response.data.BasePackageMapResponse;
import com.bong.repository.mappers.ProcessMapper;
import com.bong.repository.mongo.BasePackageRepository;
import com.bong.repository.mongo.EventChannelRepository;
import com.bong.repository.mongo.MappingChannelRepository;
import com.bong.repository.mongo.PayChannelRepository;
import com.bong.utils.FormatUtils;
import com.bong.utils.ResponseUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Stream;

/**
 * Created by ibong-gi on 2017. 1. 13..
 */
@Slf4j
@Service
public class DataProcessServiceImpl implements DataProcessService{

    @Autowired CommonProperty commonProperty;
    @Autowired ProcessMapper processMapper;
    @Autowired PayChannelRepository payChannelRepository;
    @Autowired EventChannelRepository eventChannelRepository;
    @Autowired BasePackageRepository basePackageRepository;
    @Autowired MappingChannelRepository mappingChannelRepository;
    @Autowired AppConfig appConfig;
    @Autowired RedisClusterService redisClusterService;

    /**
     * TV_PACKAGE DATA SET 갱신
     * @param dataSetApiParam
     * @return TV_PACKAGE : 시청가능 채널권한 Map
     */
    public BasePackageMapResponse setBasePackageMapData(DataSetApiParam dataSetApiParam){
        BasePackageMapResponse setBasePackageMapResponse = new BasePackageMapResponse();
        Map<String, String> resultBaseMap = new TreeMap<>();

        try{
            if(commonProperty.getApiKey().equals(dataSetApiParam.getApi_key())){
                basePackageRepository.deleteAll();
                log.debug(Constants.BASE_PACKAGE_MAP + " delete all");

                List<BasePackageInfo> basePackageList = processMapper.getPackageChannel();
                basePackageList.forEach(basePackageMap->{
                    resultBaseMap.put(basePackageMap.getTv_package(), basePackageMap.getCh_no());
                });

                basePackageRepository.save(new BasePackageMapCollection(Constants.BASE_PACKAGE_MAP, resultBaseMap));

                ResponseUtils.setSuccessResponse(setBasePackageMapResponse);
            }else{
                ResponseUtils.setFailResponse(setBasePackageMapResponse);
            }
        } catch (Exception e){
            ResponseUtils.setFailResponse(setBasePackageMapResponse);
            e.printStackTrace();
        }

        setBasePackageMapResponse.setBasePackageMap(resultBaseMap);

        return setBasePackageMapResponse;
    }

    /**
     * 유료/EVENT 채널 구매자 FULL DATA SET
     * @param dataSetApiParam
     * @return CommonResponse
     */
    public CommonResponse setPayUserChannelData(DataSetApiParam dataSetApiParam){
        CommonResponse commonResponse = new CommonResponse();

        try {
            if (commonProperty.getApiKey().equals(dataSetApiParam.getApi_key())) {
                String channel_type = StringUtils.defaultString(dataSetApiParam.getSet_type(), "");

                int result_size = 0;
                if ("pay".equals(channel_type)){
                    payChannelRepository.deleteAll();
                    List<PayChannelDataCollection> channelList = processMapper.getAllPayData(commonProperty.getTvPackageList());
                    result_size = channelList.size();
                    payChannelRepository.save(channelList);
                }else if("event".equals(channel_type)){
                    eventChannelRepository.deleteAll();
                    List<EventChannelDataCollection> channelList = processMapper.getEventData();
                    result_size = channelList.size();
                    eventChannelRepository.save(channelList);
                }else{
                    ResponseUtils.setFailResponse(commonResponse);
                }

                if(!"".equals(channel_type)){
                    log.info(channel_type+ " collection :: " + result_size + " data set finish");
                    ResponseUtils.setSuccessResponse(commonResponse);
                }
            } else {
                ResponseUtils.setFailResponse(commonResponse);
            }
        } catch (Exception e){
            ResponseUtils.setFailResponse(commonResponse);
            e.printStackTrace();
        }
        return commonResponse;
    }

    /**
     * Channel Mapping Data Set
     * @param dataSetApiParam
     * @return CommonResponse
     */
    public CommonResponse setMappingChannelData(DataSetApiParam dataSetApiParam){
        CommonResponse commonResponse = new CommonResponse();

        try {
            if (commonProperty.getApiKey().equals(dataSetApiParam.getApi_key())) {
                mappingChannelRepository.deleteAll();
                log.debug(Constants.MAPPING_CH_DATA + " delete all");
//                MappingChannelDataCollection ch_list = new MappingChannelDataCollection(Constants.MAPPING_CH_DATA, processMapper.getMappingChannelList());
//                appConfig.setMappingList(ch_list.getCh_list());
//                log.info("getMappingchannelData :" + appConfig.getMappingList());
                mappingChannelRepository.save(new MappingChannelDataCollection(Constants.MAPPING_CH_DATA, processMapper.getMappingChannelList()));
            } else {
                ResponseUtils.setFailResponse(commonResponse);
            }
        } catch (Exception e){
            ResponseUtils.setFailResponse(commonResponse);
            e.printStackTrace();
        }
        return commonResponse;
    }

    /**
     * 전체 맵핑 채널 생성 (사용시 주의!)
     * @param dataSetApiParam
     * @return
     */
    public CommonResponse makeFullMappingData(DataSetApiParam dataSetApiParam){
        CommonResponse commonResponse = new CommonResponse();

        log.info(" ### redis flush start ###");
        redisClusterService.flushDbVO();
        log.info(" ### redis flush finish ###");

        makeFullData(processMapper.getAllStbs()
                , FormatUtils.stringToArray(mappingChannelRepository.findOne(Constants.MAPPING_CH_DATA).getCh_list()));
        return commonResponse;
    }

    /**
     * MakeFullDataProcess
     */
    private void makeFullData(List<StbUserInfo> allStbs, String[] mappingChList){
        log.info("search all stb count : " + allStbs.size());

        StringBuilder resultStr = new StringBuilder(mappingChList.length+1);
        PayChannelDataCollection payChStr;
        BasePackageMapCollection baseChStr;
        List<String> baseChList;
        List<String> payChList;
        int count;

        for(StbUserInfo stb : allStbs){
            count = 0;
            resultStr.setLength(0);
            baseChStr = basePackageRepository.findOne(Constants.BASE_PACKAGE_MAP);
            payChStr = payChannelRepository.findById(stb.getStb_id());

            baseChList = baseChStr != null
                    ? FormatUtils.stringToList(baseChStr.getPackage_map().get(stb.getTv_package())) : new ArrayList<>();
            payChList = payChStr != null
                    ? FormatUtils.stringToList(payChStr.getCh_no()) : new ArrayList<>();

            for(String ch : mappingChList){
                if(baseChList.contains(ch) || payChList.contains(ch)){
                    resultStr.append(ch+":1");
                }else{
                    resultStr.append(ch+":0");
                }

                count++;

                if(mappingChList.length != count){
                    resultStr.append(",");
                }
            }
            redisClusterService.setVO(stb.getStb_id()
                    , stb.getMac_address() + "#" + stb.getTv_package() + "#" + resultStr.toString());
        }
    }

    /**
     * Test Data Set Function
     * */
    private void setTestData(){
        String value;
        for(int i=0; i< 10000; i++){
            value = Integer.toString(i);
            redisClusterService.setVO(value, value);
        }
    }
}
