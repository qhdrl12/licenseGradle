package com.bong.svc;

import com.bong.com.CommonProperty;
import com.bong.domain.request.StbInfoSearchParam;
import com.bong.domain.data.LicenseInfoOrigin;
import com.bong.domain.response.license.LicenseResponse;
import com.bong.domain.response.license.LicenseResponseList;
import com.bong.domain.data.StbUserDetailInfo;
import com.bong.repository.mappers.StbMapper;
import com.bong.utils.FormatUtils;
import com.bong.utils.JsonUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Service
public class LicenseServiceImpl implements LicenseService {
    @Autowired StbMapper stbMapper;
    @Autowired RedisClusterService redisClusterService;
    @Autowired RestTemplate restTemplate;
    @Autowired CommonProperty comConfig;

    @Override
    public LicenseResponseList getStbStatus(StbInfoSearchParam stbInfoSearchParam, String[] fields) {
        LicenseResponseList licenseResponseList = new LicenseResponseList();
        try{
            String licenseKey;
            String licenseInfo;
            String access;
            String result = "YES";
            String[] licenseInfoArr;

            licenseInfo = redisClusterService.getVO(stbInfoSearchParam.getStb_id());

            if(licenseInfo != null && (stbInfoSearchParam.getMac_address().equals(licenseInfo.substring(0, licenseInfo.indexOf("#"))))){
                access = "0";
                licenseKey = FormatUtils.licenseKeyToReturnFormat(substrLicenseKey(licenseInfo));
            }else{
                access = "1";
                licenseKey = getLicenseInfoDB(stbInfoSearchParam);
                if(licenseKey == null) {
                    /**
                     *  라이센스에 처리되지 않는 IPTV_STATUS_CODE *
                     *  */
                    result = "FAIL";
                    licenseKey = "ERR|keyisnull";
                }else{
                    licenseInfoArr = licenseKey.split("#");

                    if(licenseInfoArr.length > 1){
                        if(2 < Integer.parseInt(licenseInfoArr[0]) && licenseInfoArr[1].indexOf("err") == -1){
                            redisClusterService.setVO(stbInfoSearchParam.getStb_id()
                                    , stbInfoSearchParam.getMac_address() + "#" + licenseKey);
                            licenseKey = FormatUtils.licenseKeyToReturnFormat(substrLicenseKey(licenseKey));
                        }
                    }else{
                        /**
                         * 1 - 미 가입 (IPTVNOTACCOUNT)
                         * 2 - 청약 중 (Default Channel List)
                         * */
                        if("1".equals(licenseInfoArr[0])){
                            result = "FAIL";
                            licenseKey = "ERR|IPTVNOACCOUNT";
                        }else if("2".equals(licenseInfoArr[0])){
                            licenseKey = comConfig.getDefaultLicenseKey();
                        }
                    }
                }
            }

            if(fields == null || fields.length == 0){
                licenseResponseList.setLicenseResponseList(new LicenseResponse(licenseKey, access, result));
            }else{
                licenseResponseList.setLicenseResponseList(
                        (LicenseResponse) JsonUtils.jsonFilter("licenseJsonFilter", fields, new LicenseResponse(licenseKey, access, result), LicenseResponse.class)
                );
            }
        }catch(Exception e){
            log.error("Get Error [ stb_id : " + stbInfoSearchParam.getStb_id() + " ]" + e);
            licenseResponseList.setLicenseResponseList(new LicenseResponse("err", "1", "EXCEPTION"));
        }
        return licenseResponseList;
    }

    private String getLicenseInfoDB(StbInfoSearchParam stbInfoSearchParam){
        StbUserDetailInfo stbUserDetailInfo = stbMapper.getStbInfo(stbInfoSearchParam);

        String result = null;

        if(!ObjectUtils.isEmpty(stbUserDetailInfo)){
            String iptvStatusCode   = StringUtils.defaultString(stbUserDetailInfo.getIptv_status_code(), "");
            String tvPackage        = StringUtils.defaultString(stbUserDetailInfo.getTv_package(), "");
            if("".equals(iptvStatusCode)){
                result = "1#";
            }else if("0".equals(iptvStatusCode)){
                result = "2#";
            }else if("1".equals(iptvStatusCode)){
                LicenseInfoOrigin licenseInfoOrigin = stbMapper.getLicenseInfo(stbInfoSearchParam);
                result = licenseInfoOrigin != null ? tvPackage + "#" + licenseInfoOrigin.getLicense_info() : "";
            }else{
                result = null;
            }
        }
        return result;
    }

    private String substrLicenseKey(String licenseKey){
        return licenseKey.substring(licenseKey.lastIndexOf("#")+1);
    }

    /**
     * 임시 RestTemplate 사용 조회
     * @param stbInfoSearchParam
     */
    public void callRestTemplate(StbInfoSearchParam stbInfoSearchParam){
        JSONObject requestJson = new JSONObject();
        requestJson.put("stb_id", stbInfoSearchParam.getStb_id());
        requestJson.put("mac_address", stbInfoSearchParam.getMac_address());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> requestEntity = new HttpEntity<>(requestJson.toString(), headers);

        String url = "http://localhost:8080/iptvchlicense/search";
        LicenseResponseList response = restTemplate.postForObject(url, requestEntity, LicenseResponseList.class);
        log.info("result log : " + response);
    }
}
