package com.bong.svc;

import com.bong.domain.request.DataSetApiParam;
import com.bong.domain.response.CommonResponse;
import com.bong.domain.response.data.BasePackageMapResponse;

/**
 * Created by ibong-gi on 2017. 1. 13..
 */
public interface DataProcessService {
    BasePackageMapResponse setBasePackageMapData(DataSetApiParam apiParam);
    CommonResponse setPayUserChannelData(DataSetApiParam apiParam);
    CommonResponse setMappingChannelData(DataSetApiParam apiParam);
    CommonResponse makeFullMappingData(DataSetApiParam apiParam);
}
