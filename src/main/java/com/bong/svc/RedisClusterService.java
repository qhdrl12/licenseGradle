package com.bong.svc;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.*;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;


/**
 * Created by ibong-gi on 2017. 1. 19..
 */

@Slf4j
@Service
public class RedisClusterService {

    @Autowired
    private RedisTemplate<String, String> redisTemplate;
//    private RedisAtomicLong nodeIdCounter;
    private ValueOperations<String, String> valueOps;
    private ListOperations<String, String> listOps;

    @PostConstruct
    public void init(){
//        nodeIdCounter = new RedisAtomicLong("MY_KEY_FOR_QUEUE", redisTemplate.getConnectionFactory());
        valueOps = redisTemplate.opsForValue();
        listOps = redisTemplate.opsForList();
    }

    public String getVO(String key){
        return valueOps.get(key);
    }

    public void setVO(String key, String value){
        valueOps.set(key, value);
    }

    public void flushDbVO(){
        try{
            redisTemplate.execute((RedisCallback<Object>) connection -> {
                connection.flushDb();
                return null;
            });
        }catch(Exception e){
            log.warn("cache flush fail.", e);
        }
    }

    public String getRandomkey(){
        return redisTemplate.randomKey();
    }

    public void testMethod(){

    }
}
