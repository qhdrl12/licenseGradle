package com.bong.com;

import lombok.Data;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created by ibong-gi on 2016. 12. 7..
 */

@Component
@Data
@ConfigurationProperties(prefix = "spring.redis")
public class RedisProperty {
    private RedisProperties.Cluster cluster;
    private RedisProperties.Pool pool;
    private int timeout;
}

