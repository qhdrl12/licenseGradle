package com.bong.com;

/**
 * Created by ibong-gi on 2017. 1. 16..
 */
public final class Constants {
    public static final String BASE_PACKAGE_MAP = "basePackageMap";
    public static final String PAY_CHANNEL_DATA = "payChannelData";
    public static final String EVENT_CHANNEL_DATA = "eventChannelData";
    public static final String MAPPING_CH_DATA = "mappingChannelData";
}
