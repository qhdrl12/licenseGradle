package com.bong.com;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Cloud Common Property
 * chliconf-local.yml
 */
@Slf4j
@Component
@Data
@ConfigurationProperties(prefix = "common")
@RefreshScope
public class CommonProperty {
    /** 청약 중 기본 키 제공 **/
    private String defaultLicenseKey;
    private String apiKey;
    private List<String> tvPackageList = new ArrayList<>();
    private Map<String, Object> rest = new HashMap<>();
    private Map<String, Object> sync = new HashMap<>();
    private Map<String, Object> bypass = new HashMap<>();

    @Override
    public String toString(){
        return "BasePackageInfo{" +
                "defaultLicenseKey='" + defaultLicenseKey + '\'' +
                "tvPackageList='" + tvPackageList + '\'' +
                "bypass='" + bypass + '\'' +
                '}';
    }
}
