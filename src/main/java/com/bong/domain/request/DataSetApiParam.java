package com.bong.domain.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.ToString;

/**
 * Created by ibong-gi on 2017. 1. 11..
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.ALWAYS)
@Data
@ToString(callSuper = true)
public class DataSetApiParam extends StbInfoSearchParam{

    @JsonProperty("api_key") private String api_key;
    @JsonProperty("set_type") private String set_type;
}
