package com.bong.domain.response.data;

import com.bong.domain.response.CommonResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.ToString;

import java.util.Map;

/**
 * Created by ibong-gi on 2017. 1. 11..
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.ALWAYS)
@Data
@ToString(callSuper = true)
public class BasePackageMapResponse extends CommonResponse {
    @JsonProperty("base_package_map") private Map<String, String> basePackageMap;
}
