package com.bong.domain.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * Created by ibong-gi on 2017. 1. 18..
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.ALWAYS)
@Data
public class StbUserInfo implements Serializable{

    @JsonProperty("stbId")
    private String stb_id;
    @JsonProperty("tvPackage")
    private String tv_package;
    @JsonProperty("mac_address")
    private String mac_address;

    @Override
    public String toString(){
        return "Stb{" +
                "stb_id='" + stb_id + '\'' +
                "tv_package='" + tv_package + '\'' +
               '}';
    }
}
