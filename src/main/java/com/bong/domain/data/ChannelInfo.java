package com.bong.domain.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.data.annotation.Id;

import java.io.Serializable;

/**
 * Created by ibong-gi on 2017. 1. 13..
 */

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.ALWAYS)
@Data
public class ChannelInfo implements Serializable{

    @Id
    @JsonProperty("stbId") String stb_id;
    @JsonProperty("chNo") String ch_no;
    @JsonProperty("ddApplyEnd") String dd_apply_end;

    @Override
    public String toString(){
        return "ChannelInfo{" +
                "stb_id='" + stb_id + '\'' +
                "ch_no='" + ch_no + '\'' +
                "dd_appy_end='" + dd_apply_end + '\'' +
                '}';
    }
}
