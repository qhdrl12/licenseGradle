package com.bong.domain.data.mongo;

import com.bong.com.Constants;
import com.bong.domain.data.ChannelInfo;
import lombok.ToString;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by ibong-gi on 2017. 1. 13..
 */

@Document(collection = Constants.EVENT_CHANNEL_DATA)
@ToString(callSuper = true)
public class EventChannelDataCollection extends ChannelInfo{
}
