package com.bong.domain.data.mongo;

import com.bong.com.Constants;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.Map;

/**
 * Created by ibong-gi on 2017. 1. 13..
 */

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.ALWAYS)
@Document(collection = Constants.BASE_PACKAGE_MAP)
@Data
public class BasePackageMapCollection implements Serializable{

    @Id
    String id;
    Map<String, String> package_map;

    public BasePackageMapCollection(){}

    public BasePackageMapCollection(String id, Map<String, String> package_map){
        this.id = id;
        this.package_map = package_map;
    }

    @Override
    public String toString(){
        return "BasePackageMInfo{" +
                "id='" + id + '\'' +
                "package_map='" + package_map + '\'' +
                '}';
    }
}
