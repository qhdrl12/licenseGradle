package com.bong.domain.data.mongo;

import com.bong.com.Constants;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import org.springframework.boot.context.embedded.MimeMappings;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

/**
 * Created by ibong-gi on 2017. 1. 13..
 */

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.ALWAYS)
@Document(collection = Constants.MAPPING_CH_DATA)
@Data
public class MappingChannelDataCollection implements Serializable{

    @Id
    String id;
    String ch_list;

    public MappingChannelDataCollection(String id, String ch_list){
        this.id = id;
        this.ch_list = ch_list;
    }

    @Override
    public String toString(){
        return "ChannelInfo{" +
                "id='" + id + '\'' +
                "ch_list='" + ch_list + '\'' +
                '}';
    }
}
