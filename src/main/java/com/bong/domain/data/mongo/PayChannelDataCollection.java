package com.bong.domain.data.mongo;

import com.bong.com.Constants;
import com.bong.domain.data.ChannelInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

/**
 * Created by ibong-gi on 2017. 1. 13..
 */

@Document(collection = Constants.PAY_CHANNEL_DATA)
@ToString(callSuper = true)
public class PayChannelDataCollection extends ChannelInfo{
}
