package com.bong.domain.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

/**
 * Created by ibong-gi on 2017. 1. 13..
 */

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.ALWAYS)
@Data
public class BasePackageInfo implements Serializable{

    @JsonProperty("tvPackage") String tv_package;
    @JsonProperty("chNo") String ch_no;
    @JsonProperty("ddApplyEnd") String dd_apply_end;

    @Override
    public String toString(){
        return "BasePackageInfo{" +
                "tv_package='" + tv_package + '\'' +
                "dd_apply_end='" + dd_apply_end + '\'' +
                "ch_no='" + ch_no + '\'' +
                '}';
    }
}
