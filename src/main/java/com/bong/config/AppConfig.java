package com.bong.config;


import com.bong.com.CommonProperty;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;

/**
 * Created by ibong-gi on 2016. 12. 19..
 */

@Slf4j
@Configuration
public class AppConfig {

    @Autowired CommonProperty commonProperty;

    private List<String> mapping_list = new ArrayList<>();
    private List<String> ips = new ArrayList<>();

    @Bean
    public RestTemplate restTemplate(){
        RestTemplate restTemplate = new RestTemplate(clientHttpRequestFactory());

        int index = 0;
        for(HttpMessageConverter messageConverter : restTemplate.getMessageConverters()) {
            if (messageConverter instanceof StringHttpMessageConverter) {
                break;
            }
            index++;
        }

        List<HttpMessageConverter<?>> httpMessageConverters = restTemplate.getMessageConverters();
        httpMessageConverters.remove(index);
        httpMessageConverters.add(index, new StringHttpMessageConverter(Charset.forName("UTF-8")));

        return restTemplate;
    }

    private ClientHttpRequestFactory clientHttpRequestFactory(){
        HttpComponentsClientHttpRequestFactory factory =
                new HttpComponentsClientHttpRequestFactory();

        factory.setReadTimeout((int) commonProperty.getRest().get("template-read-timeout"));
        factory.setConnectTimeout((int) commonProperty.getRest().get("template-connect-timeout"));
        return factory;
    }

    @PostConstruct
    private void init(){
        try{
            /**
             *  서버 IP 주소 ips 저장
             * */
            for(Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();){
                NetworkInterface ni = en.nextElement();
                for(Enumeration<InetAddress> enumIpAddr = ni.getInetAddresses(); enumIpAddr.hasMoreElements();){
                    InetAddress inetAddress = enumIpAddr.nextElement();

                    if(!inetAddress.isLoopbackAddress() && !inetAddress.isLinkLocalAddress()){
                        ips.add("http://" + inetAddress.getHostAddress().toString() + ":8080");
                    }
                }
            }
        }catch (Exception e){
            log.error("init Exception : " + e);
        }
    }

    public List<String> getMappingList(){
        return mapping_list;
    }

    public void setMappingList(String mappingList){
        try {
            this.mapping_list = Arrays.asList(mappingList);
        }catch (Exception e){
            this.mapping_list = new ArrayList<>();
            log.error("setMappingList Error : " + e);
        }
    }
}
