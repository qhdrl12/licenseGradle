package com.bong.config;
//
import com.bong.com.RedisProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisClusterConfiguration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import redis.clients.jedis.*;

//
///**
// * Created by ibong-gi on 2016. 12. 21..
// */
@Configuration
public class RedisConfig {

    @Autowired RedisProperty redisProperty;

//    private Set<HostAndPort> jedisClusterNodes(){
//        Set<HostAndPort> hostAndPorts = new HashSet<>();
//        String[] nodeArr;
//
//        for(String node : redisProperty.getCluster().getNodes()){
//            nodeArr = node.split(":");
//            if(nodeArr.length > 1) {
//                hostAndPorts.add(new HostAndPort(nodeArr[0].trim(), Integer.parseInt(nodeArr[1].trim())));
//            }
//        }
//
//        return hostAndPorts;
//    }
//
//    private GenericObjectPoolConfig initPoolConfiguration(){
//        GenericObjectPoolConfig config = new GenericObjectPoolConfig();
//        config.setMaxTotal(redisProperty.getPool().getMaxActive());
//        config.setMaxIdle(redisProperty.getPool().getMaxIdle());
//        config.setMinIdle(redisProperty.getPool().getMinIdle());
//        config.setMaxWaitMillis(redisProperty.getPool().getMaxWait());
//
//        return config;
//    }

    public JedisPoolConfig jedisPoolConfig(){
        JedisPoolConfig config = new JedisPoolConfig();
        RedisProperties.Pool props = redisProperty.getPool();
        config.setMaxTotal(props.getMaxActive());
        config.setMaxIdle(props.getMaxIdle());
        config.setMinIdle(props.getMinIdle());
        config.setMaxWaitMillis(props.getMaxWait());
        return config;
    }

    @Bean
    public RedisConnectionFactory connectionFactory(){
        return new JedisConnectionFactory(
                new RedisClusterConfiguration(redisProperty.getCluster().getNodes()), jedisPoolConfig());
    }
}
