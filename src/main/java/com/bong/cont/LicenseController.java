package com.bong.cont;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * Created by qhdrl on 2016-12-01.
 */

@Slf4j
@Controller
public class LicenseController {

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String indexPage(){
        return "index";
    }
}
