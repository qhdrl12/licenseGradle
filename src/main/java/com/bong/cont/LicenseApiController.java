package com.bong.cont;

import com.bong.domain.request.StbInfoSearchParam;
import com.bong.domain.response.license.LicenseResponseList;
import com.bong.svc.LicenseService;
import com.sun.media.jfxmedia.Media;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

/**
 * Created by ibong-gi on 2016. 12. 21..
 */

@Slf4j
@RestController
@RequestMapping(value = "/iptvchlicense")
public class LicenseApiController {

    @Autowired LicenseService licenseService;

    @RequestMapping(value = "/stbs/{stb_id}/{mac_address}"
            , method = RequestMethod.GET
            , consumes = MediaType.APPLICATION_JSON_VALUE
            , produces = MediaType.APPLICATION_XML_VALUE)
    public LicenseResponseList searchLicenseKey(@ModelAttribute StbInfoSearchParam stbInfoSearchParam
        ,@RequestParam(value="fields", required = false) String[] fields) throws Exception{
        log.info("request body : {}, fields : {}", stbInfoSearchParam, fields);
        return licenseService.getStbStatus(stbInfoSearchParam, fields);
    }

//    @RequestMapping(value = "/search", method = RequestMethod.POST
//            , consumes = MediaType.APPLICATION_JSON_VALUE
//            , produces = MediaType.APPLICATION_XML_VALUE)
//    public LicenseResponseList searchLicenseKey(@RequestBody StbInfoSearchParam stbInfoSearchParam) throws Exception{
//        log.info("request body : {}", stbInfoSearchParam);
//        return licenseService.getStbStatus(stbInfoSearchParam);
//    }

//    @RequestMapping(value = "/newSearch", method = RequestMethod.POST
//            , consumes = MediaType.APPLICATION_JSON_VALUE
//            , produces = MediaType.APPLICATION_XML_VALUE)
//    public LicenseResponseList newSearchLicenseKey(@RequestBody StbInfoSearchParam stbInfoSearchParam) throws Exception{
//        log.info("request body : {}", stbInfoSearchParam);
//        return licenseService.getStbStatus(stbInfoSearchParam);
//    }

    /***
     * RestTempalte 조회 임시 IF
     * @param stbInfoSearchParam
     * @throws Exception
     */
    @RequestMapping(value = "/api/stb/{stb_id}/{mac_address}", method = RequestMethod.GET)
    public void getRestTemplate(@ModelAttribute StbInfoSearchParam stbInfoSearchParam) throws Exception{
        log.info("getRestTemplate params : {}", stbInfoSearchParam);

        licenseService.callRestTemplate(stbInfoSearchParam);
    }
}
