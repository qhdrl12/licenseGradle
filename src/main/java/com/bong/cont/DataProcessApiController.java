package com.bong.cont;

import com.bong.domain.request.DataSetApiParam;
import com.bong.domain.response.CommonResponse;
import com.bong.domain.response.data.BasePackageMapResponse;
import com.bong.svc.DataProcessService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by ibong-gi on 2017. 1. 13..
 */

@Slf4j
@RestController
@RequestMapping(value = "/process/")
public class DataProcessApiController {

    @Autowired DataProcessService dataProcessService;

    @RequestMapping(value = "/setBasePackageMapData", method = RequestMethod.POST)
    public BasePackageMapResponse setBasePackageMapData(@RequestBody DataSetApiParam dataSetApiParam) throws Exception{
        log.info("request body : {}", dataSetApiParam);
        return dataProcessService.setBasePackageMapData(dataSetApiParam);
    }

    @RequestMapping(value = "/setPayUserChannelData", method = RequestMethod.POST)
    public CommonResponse setPayUserChannelData(@RequestBody DataSetApiParam dataSetApiParam) throws Exception{
        log.info("request body : {}", dataSetApiParam);
        return dataProcessService.setPayUserChannelData(dataSetApiParam);
    }

    @RequestMapping(value = "/setMappingChannelData", method = RequestMethod.POST)
    public CommonResponse setMappingChannelData(@RequestBody DataSetApiParam dataSetApiParam) throws Exception{
        log.info("request body : {}", dataSetApiParam);
        return dataProcessService.setMappingChannelData(dataSetApiParam);
    }

    @RequestMapping(value = "/makeFullMappingData", method = RequestMethod.POST)
    public CommonResponse makeFullMappingData(@RequestBody DataSetApiParam dataSetApiParam) throws Exception{
        log.info("request body : {}", dataSetApiParam);
        return dataProcessService.makeFullMappingData(dataSetApiParam);
    }
}
